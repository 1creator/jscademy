function toggleModal(modal, value) {
    let bodyWidth = document.body.offsetWidth;
    if (value === undefined) {
        value = !modal.classList.contains('show');
    }
    modal.classList.toggle('show', value);
    document.documentElement.style.overflowY = value ? 'hidden' : 'auto';

    document.documentElement.style.paddingRight = value ? (document.body.offsetWidth - bodyWidth + 'px') : 0;
}

let modals = document.getElementsByClassName('modal');
for (let modal of modals) {
    document.querySelectorAll(`[data-target=${modal.id}], [href=${modal.id}]`)
        .forEach(toggler => toggler.addEventListener('click', function () {
            toggleModal(document.getElementById(this.dataset.target));
        }));

    modal.querySelectorAll('[data-action="close"]').forEach(item => {
        item.addEventListener('click', function (event) {
            event.preventDefault();
            toggleModal(modal, false);
        });
    });
}
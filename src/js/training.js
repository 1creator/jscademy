import bsn from 'bootstrap.native/dist/bootstrap-native-v4';

function Training() {
    this.init = function (files) {
        let tabMenu = document.getElementsByClassName('training-redactor__tabs')[0];
        let tabContent = document.getElementsByClassName('tab-content')[0];

        for (let file in files) {
            let escapedName = file.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '_');

            let li = document.createElement('li');
            li.class = 'nav-item';
            li.innerHTML = `<a class="nav-link" data-toggle="tab" href="#${escapedName}" role="tab">${file}</a>`;
            let a = li.children[0];
            tabMenu.appendChild(li);

            let tabPane = document.createElement('div');
            tabPane.classList.toggle('tab-pane', true);
            tabPane.id = escapedName;
            tabContent.appendChild(tabPane);

            files[file].tab = a;
            files[file].tabPane = tabPane;
            new bsn.Tab(a);

            let editor = ace.edit(files[file].tabPane, {
                mode: files[file].mode,
                minLines: 20,
                theme: "ace/theme/dracula",
                autoScrollEditorIntoView: true,
            });
            editor.setValue(files[file].content, 1);
        }

        let firstTabKey = Object.keys(files)[0];
        files[firstTabKey].tab.classList.toggle('active', true);
        files[firstTabKey].tabPane.classList.toggle('show', true);
        files[firstTabKey].tabPane.classList.toggle('active', true);
    }
};

window.training = new Training();



const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath('dist');
// mix.config.fileLoaderDirs.fonts = 'fonts';
mix
    .options({processCssUrls: false})
    .extract(['swiper', 'bootstrap.native', 'simplebar'])
    .js('src/js/app.js', 'dist/js/')
    .js('src/js/training.js', 'dist/js/')
    .sass('src/scss/vendor.scss', 'dist/css/')
    .sass('src/scss/app.scss', 'dist/css/');

mix.disableSuccessNotifications();